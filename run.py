from donationcalc import app
from donationcalc import donationconfig
import donationcalc.models
import argparse

def parseArgs(parser):
    parser.add_argument("-r", "--rebalance", dest = "rebalance", help = "balance the monthly values", action="store_true")
    return parser.parse_args()

parser = argparse.ArgumentParser()
args = parseArgs(parser)

if __name__ == '__main__':

    if args.rebalance:
        donconf = donationconfig.donationconfig()
        donconf.rebalance()
        sender = donationcalc.models.sender()
        donors = []
        f = open("donors.txt", "r")

        for line in f.readlines():
            linesplit = line.split(",")
            newdonor = donationcalc.models.donor(email = linesplit[0], monthly = linesplit[1])
            donors.append(newdonor)
        f.close()

        donationcalc.models.rebalanceall(sender,donors)
        exit()

    app.run(debug=False)

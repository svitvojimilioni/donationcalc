from flask import render_template, request, redirect
from donationcalc import app
import donationcalc.models
import donationcalc.donationconfig

@app.route('/donations/subscribe/', methods=['POST', 'GET'])
def subscribe():
    config = donationcalc.donationconfig.donationconfig()
    currency = config.currency

    sender = donationcalc.models.sender()
    currentlypledged = sender.pledged()
    currentpercent = round(currentlypledged/int(config.monthlycost), 2)

    if request.method == 'GET':
        return render_template('subscribe.html', monthlycost = int(config.monthlycost), pledged = currentlypledged, percent = currentpercent)
    elif request.method == 'POST':
        donoremail = request.form['email']
        donormonthly = request.form['monthly']

        newdonor = donationcalc.models.donor(email = donoremail, monthly = donormonthly)

        f = open("donors.txt","a")
        f.write(f"{donoremail},{donormonthly}\n")
        f.close()

        return f"You subscribed to monthly reminder to donate {donormonthly} {currency}"
    else:
        return 'HTTP request method not recogniezed'
